package model;

import java.io.Serializable;

public class Book implements Serializable {

	private int id;
	private Author author;
	private int author_id;
	private Bookshelf bookshelf;
	private int bookshelf_id;
	private int category_id;
	private Publisher publisher;
	private int publisher_id;
	private String title;
	private String outline;

	// 全てのデータをセットするコンストラクタ
	public Book(int id, int author_id, int bookshelf_id, int category_id, int publisher_id, String title,
			String outline) {
		this.id = id;
		this.author_id = author_id;
		this.bookshelf_id = bookshelf_id;
		this.category_id = category_id;
		this.publisher_id = publisher_id;
		this.title = title;
		this.outline = outline;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getAuthor_id() {
		return author_id;
	}

	public void setAuthor_id(int author_id) {
		this.author_id = author_id;
	}

	public int getBookshelf_id() {
		return bookshelf_id;
	}

	public void setBookshelf_id(int bookshelf_id) {
		this.bookshelf_id = bookshelf_id;
	}

	public int getCategory_id() {
		return category_id;
	}

	public void setCategory_id(int category_id) {
		this.category_id = category_id;
	}

	public int getPublisher_id() {
		return publisher_id;
	}

	public void setPublisher_id(int publisher_id) {
		this.publisher_id = publisher_id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getOutline() {
		return outline;
	}

	public void setOutline(String outline) {
		this.outline = outline;
	}

	public Author getAuthor() {
		return this.author;
	}

	public void setAuthor(Author author) {
		this.author = author;
	}

	public Bookshelf getBookshelf() {
		return this.bookshelf;
	}

	public void setBookshelf(Bookshelf bookshelf) {
		this.bookshelf = bookshelf;
	}

	public Publisher getPublisher() {
		return this.publisher;
	}

	public void setPublisher(Publisher publisher) {
		this.publisher = publisher;
	}
}
