package model;

import java.io.Serializable;

public class BookCirculation implements Serializable {

	private int id;
	private User user;
	private int user_id;
	private Book book;
	private int book_id;
	private int circulation_status;
	private String circulation_date;
	private String return_period;
	private String return_date;
	private String create_date;
	private String update_date;

	public BookCirculation(int id, int user_id, int book_id, int circulation_status, String circulation_date,
			String return_period, String return_date, String create_date, String update_date) {

		this.id = id;
		this.user_id = user_id;
		this.book_id = book_id;
		this.circulation_status = circulation_status;
		this.circulation_date = circulation_date;
		this.return_period = return_period;
		this.return_date = return_date;
		this.create_date = create_date;
		this.update_date = update_date;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public int getBook_id() {
		return book_id;
	}

	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}

	public int getCirculation_status() {
		return circulation_status;
	}

	public void setCirculation_status(int circulation_status) {
		this.circulation_status = circulation_status;
	}

	public String getCirculation_date() {
		return circulation_date;
	}

	public void setCirculation_date(String circulation_date) {
		this.circulation_date = circulation_date;
	}

	public String getReturn_period() {
		return return_period;
	}

	public void setReturn_period(String return_period) {
		this.return_period = return_period;
	}

	public String getReturn_date() {
		return return_date;
	}

	public void setReturn_date(String return_date) {
		this.return_date = return_date;
	}

	public String getCreate_date() {
		return create_date;
	}

	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}

	public String getUpdate_date() {
		return update_date;
	}

	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Book getBook() {
		return this.book;
	}

	public void setBook(Book book) {
		this.book = book;
	}

}
