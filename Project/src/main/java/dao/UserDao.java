package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

public class UserDao {

	public User getUser(int userId) {
		Connection conn = null;

		List<User> userList = new ArrayList<User>();
		try {
			conn = DBManager.getConnection();

			String sql = "select user.* from book_circulation inner join user on book_circulation.user_id = user.id WHERE user.id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, userId);
			ResultSet rs = ps.executeQuery();

			userList = ResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return userList.get(0);
	}

	public User findByLoginInfo(String mail) {
		Connection conn = null;
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE mail = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, mail);
			ResultSet rs = pStmt.executeQuery();

			// 登録可能
			if (!rs.next()) {
				return null;
			}
			int userIdData = rs.getInt("id");
			String mailData = rs.getString("mail");
			String passwordData = rs.getString("password");
			String nameData = rs.getString("name");
			String create_dateData = rs.getString("create_date");
			String update_dateData = rs.getString("update_date");

			return new User(userIdData, mailData, passwordData, nameData, create_dateData, update_dateData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User findByLoginInfo(String mail, String password) {
		Connection conn = null;
		try {
			// データベースへ接続、データベースからコネクションを作る
			conn = DBManager.getConnection();

			// SELECTを準備
			String sql = "SELECT * FROM user WHERE mail = ? and password = ?";

			// SELECTを実行、結果表を取得
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, mail);
			pStmt.setString(2, passwordEncrypt(password));
			ResultSet rs = pStmt.executeQuery();

			// ログイン失敗時の処理 
			if (!rs.next()) {
				return null;
			}

			// ログイン成功時の処理
			String mailData = rs.getString("mail");
			String nameData = rs.getString("name");
			return new User(mailData, nameData);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
	}

	public User findbyUserId(String userID) {
		Connection conn = null;

		List<User> userList = new ArrayList<User>();
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE id = ?";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, userID);
			ResultSet rs = pStmt.executeQuery();

			userList = ResultSet(rs);

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList.get(0);
	}

	public List<User> findByUserNameOrMail(String user_search) {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM user WHERE (mail like ? or name like ?) and is_admin != 1";

			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setString(1, "%" + user_search + "%");
			pStmt.setString(2, "%" + user_search + "%");
			ResultSet rs = pStmt.executeQuery();

			userList = ResultSet(rs);

			while (rs.next()) {
				String mailData = rs.getString("mail");
				String nameData = rs.getString("name");
				User user = new User(mailData, nameData);
				userList.add(user);
			}

		} catch (SQLException e) {
			e.printStackTrace();
			return null;
		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public List<User> findAll() {
		Connection conn = null;
		List<User> userList = new ArrayList<User>();

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// SELECT文を準備
			String sql = "SELECT * FROM user";

			// SELECTを実行し結果表を取得
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);

			while (rs.next()) {
				int idData = rs.getInt("id");
				int is_adminData = rs.getInt("is_admin");
				String mailData = rs.getString("mail");
				String passwordData = rs.getString("password");
				String nameData = rs.getString("name");
				String create_dateData = rs.getString("create_date");
				String update_dateData = rs.getString("update_date");
				User user = new User(idData, is_adminData, mailData, passwordData, nameData, create_dateData,
						update_dateData);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
			return null;

		} finally {
			// データベース切断
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
					return null;
				}
			}
		}
		return userList;
	}

	public User InsertUser(User user) {
		Connection conn = null;
		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			String password = passwordEncrypt(user.getPassword());

			// INSERT文を準備
			String sql = "INSERT INTO user (id, mail, password, name, create_date, update_date) VALUES (?,?,?,?,CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)";

			// INSERTを実行
			PreparedStatement pStmt = conn.prepareStatement(sql);
			pStmt.setInt(1, user.getId());
			pStmt.setString(2, user.getMail());
			pStmt.setString(3, password);
			pStmt.setString(4, user.getName());
			pStmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return user;
	}

	//resultsetを受け取ってUserインスタンスを作って返すメソッド
	public List<User> ResultSet(ResultSet rs) {
		List<User> userList = new ArrayList<User>();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int is_admin = rs.getInt("is_admin");
				String mail = rs.getString("mail");
				String password = rs.getString("password");
				String name = rs.getString("name");
				String create_date = rs.getString("create_date");
				String update_date = rs.getString("update_date");
				User user = new User(id, is_admin, mail, password, name, create_date, update_date);

				userList.add(user);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return userList;
	}

	// パスワードの暗号化メソッド
	public String passwordEncrypt(String source) {
		String result = null;
		Connection conn = null;
		try {
			Charset charset = StandardCharsets.UTF_8;
			String algorithm = "MD5";

			byte[] bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			result = DatatypeConverter.printHexBinary(bytes);

		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
}
