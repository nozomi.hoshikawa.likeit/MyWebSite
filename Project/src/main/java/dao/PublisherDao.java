package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Book;
import model.Publisher;

public class PublisherDao {

	public Publisher getPublisher(Book book) {
		Connection conn = null;

		List<Publisher> publisherList = new ArrayList<Publisher>();
		try {
			conn = DBManager.getConnection();

			String sql = "select publisher.* from book inner join publisher on book.publisher_id = publisher.id WHERE publisher.id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, book.getPublisher_id());
			ResultSet rs = ps.executeQuery();

			publisherList = ResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return publisherList.get(0);
	}

	//resultsetを受け取ってPublisherインスタンスを作って返すメソッド
	public List<Publisher> ResultSet(ResultSet rs) {
		List<Publisher> publisherList = new ArrayList<Publisher>();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				Publisher publisher = new Publisher(id, name);

				publisherList.add(publisher);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return publisherList;
	}
}