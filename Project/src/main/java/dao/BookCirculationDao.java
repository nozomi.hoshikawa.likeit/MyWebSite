package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.BookCirculation;
import model.User;

public class BookCirculationDao {

	public boolean CanMakeBookCirculation(String book_id) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERTを準備
			String sql = "select * from book_circulation where book_id = ? and circulation_status != 3";

			// INSERTを実行
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, book_id);
			ResultSet rs = ps.executeQuery();

			if (!rs.next()) {
				return true;
			} else {
				return false;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return false;
	}

	public List<BookCirculation> getBookCirculateHistoryList() {
		Connection conn = null;

		List<BookCirculation> bookCirculateList = new ArrayList<BookCirculation>();
		try {
			conn = DBManager.getConnection();

			String sql = "select * from book_circulation order by circulation_date DESC";

			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			bookCirculateList = ResultSet(rs);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return bookCirculateList;

	}

	public void bookReturn(String bookCirculateId) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERTを準備
			String sql = "UPDATE book_circulation SET circulation_status = 3, return_date = now() WHERE id = ?";

			// INSERTを実行
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, bookCirculateId);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public List<BookCirculation> getNotReturnedBookCirculateList() {
		Connection conn = null;

		List<BookCirculation> bookCirculateList = new ArrayList<BookCirculation>();
		try {
			conn = DBManager.getConnection();

			String sql = "select * from book_circulation where circulation_status != 3";

			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();

			bookCirculateList = ResultSet(rs);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return bookCirculateList;

	}

	public void BookCirculate(String book_id, User sessionTargetUser) {
		Connection conn = null;

		try {
			// データベースへ接続
			conn = DBManager.getConnection();

			// INSERTを準備
			String sql = "INSERT INTO book_circulation (user_id, book_id, circulation_status, circulation_date, return_period, create_date, update_date) VALUES (?,?,?,CURRENT_TIMESTAMP,DATE_ADD(now(), interval 2 week),CURRENT_TIMESTAMP,CURRENT_TIMESTAMP)";

			// INSERTを実行
			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, sessionTargetUser.getId());
			ps.setString(2, book_id);
			ps.setInt(3, 0);
			ps.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// resultsetを受け取ってBookCirculationインスタンスを作って返すメソッド
	public List<BookCirculation> ResultSet(ResultSet rs) {
		List<BookCirculation> bookCirculationList = new ArrayList<BookCirculation>();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int user_id = rs.getInt("user_id");
				int book_id = rs.getInt("book_id");
				int circulation_status = rs.getInt("circulation_status");
				String circulation_date = rs.getString("circulation_date");
				String return_period = rs.getString("return_period");
				String return_date = rs.getString("return_date");
				String create_date = rs.getString("create_date");
				String update_date = rs.getString("update_date");
				BookCirculation bookCirculation = new BookCirculation(id, user_id, book_id, circulation_status,
						circulation_date, return_period, return_date, create_date, update_date);

				bookCirculationList.add(bookCirculation);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bookCirculationList;
	}

}
