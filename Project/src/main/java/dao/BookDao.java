package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Book;

public class BookDao {

	public Book getBook(int bookId) {
		Connection conn = null;

		List<Book> bookList = new ArrayList<Book>();
		try {
			conn = DBManager.getConnection();

			String sql = "select * from book WHERE book.id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, bookId);
			ResultSet rs = ps.executeQuery();

			bookList = ResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return bookList.get(0);
	}

	public List<Book> SearchBookFromTitle(String keyword) {
		Connection conn = null;

		List<Book> bookList = new ArrayList<Book>();
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM book WHERE title like ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setString(1, "%" + keyword + "%");
			ResultSet rs = ps.executeQuery();

			bookList = ResultSet(rs);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return bookList;
	}

	public List<Book> SearchBookFromAuthor(String keyword) {
		Connection conn = null;

		List<Book> bookList = new ArrayList<Book>();
		try {
			conn = DBManager.getConnection();

			String sql = "SELECT * FROM book WHERE author like ?";

			PreparedStatement ps = conn.prepareStatement(sql);

			//			ps.setInt(1, author);
			ResultSet rs = ps.executeQuery();

			bookList = ResultSet(rs);

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return bookList;
	}

	//resultsetを受け取ってBookインスタンスを作って返すメソッド
	public List<Book> ResultSet(ResultSet rs) {
		List<Book> bookList = new ArrayList<Book>();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				int author_id = rs.getInt("author_id");
				int bookshelf_id = rs.getInt("bookshelf_id");
				int category_id = rs.getInt("category_id");
				int publisher_id = rs.getInt("publisher_id");
				String title = rs.getString("title");
				String outline = rs.getString("outline");
				Book book = new Book(id, author_id, bookshelf_id, category_id, publisher_id, title, outline);

				bookList.add(book);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bookList;
	}
}