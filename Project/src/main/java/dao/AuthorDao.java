package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Author;
import model.Book;

public class AuthorDao {

	public Author getAuthor(Book book) {
		Connection conn = null;

		List<Author> authorList = new ArrayList<Author>();
		try {
			conn = DBManager.getConnection();

			String sql = "select author.* from book inner join author on book.author_id = author.id WHERE author.id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, book.getAuthor_id());
			ResultSet rs = ps.executeQuery();

			authorList = ResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return authorList.get(0);
	}

	//resultsetを受け取ってAuthorインスタンスを作って返すメソッド
	public List<Author> ResultSet(ResultSet rs) {
		List<Author> authorList = new ArrayList<Author>();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				Author author = new Author(id, name);

				authorList.add(author);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return authorList;
	}
}