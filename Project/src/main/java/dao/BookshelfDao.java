package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Book;
import model.Bookshelf;

public class BookshelfDao {

	public Bookshelf getBookshelf(Book book) {
		Connection conn = null;

		List<Bookshelf> bookshelfList = new ArrayList<Bookshelf>();
		try {
			conn = DBManager.getConnection();

			String sql = "select bookshelf.* from book inner join bookshelf on book.bookshelf_id = bookshelf.id WHERE bookshelf.id = ?";

			PreparedStatement ps = conn.prepareStatement(sql);
			ps.setInt(1, book.getBookshelf_id());
			ResultSet rs = ps.executeQuery();

			bookshelfList = ResultSet(rs);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					e.printStackTrace();

				}
			}
		}
		return bookshelfList.get(0);
	}

	//resultsetを受け取ってBookshelfインスタンスを作って返すメソッド
	public List<Bookshelf> ResultSet(ResultSet rs) {
		List<Bookshelf> bookshelfList = new ArrayList<Bookshelf>();

		try {
			while (rs.next()) {
				int id = rs.getInt("id");
				String name = rs.getString("name");
				Bookshelf bookshelf = new Bookshelf(id, name);

				bookshelfList.add(bookshelf);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return bookshelfList;
	}
}