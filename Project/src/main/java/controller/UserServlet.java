package controller;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/UserServlet")
public class UserServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		//もしセッションがなかったら、LoginServletにリダイレクトさせる
		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// ユーザー一覧情報を取得する
		UserDao userDao = new UserDao();
		List<User> userList = userDao.findAll();

		// userListをstreamに変換 is_adminが1以外のuserを抽出 userListに代入
		userList = userList.stream()
				.filter(u -> u.getIs_admin() != 1)
				.collect(Collectors.toList());

		// リクエストスコープにユーザ一覧情報をセット
		request.setAttribute("userList", userList);

		// user.jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/user.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
