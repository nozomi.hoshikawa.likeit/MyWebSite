package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookCirculationDao;
import dao.BookDao;
import dao.UserDao;
import model.Book;
import model.BookCirculation;
import model.User;

@WebServlet("/CirculationHistoryServlet")
public class CirculationHistoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CirculationHistoryServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// もしセッションがなかったら、LoginServletにリダイレクトさせる
		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// 操作を行うユーザーの情報を受け取る
		User sessionTargetUser = (User) session.getAttribute("sessionTargetUser");
		request.setAttribute("sessionTargetUser", sessionTargetUser);

		BookCirculationDao bookCirculationDao = new BookCirculationDao();
		List<BookCirculation> bookCirculateHistoryList = bookCirculationDao.getBookCirculateHistoryList();

		UserDao userDao = new UserDao();
		BookDao bookDao = new BookDao();
		for (BookCirculation bookCirculateHistory : bookCirculateHistoryList) {
			User user = userDao.getUser(bookCirculateHistory.getUser_id());
			Book book = bookDao.getBook(bookCirculateHistory.getBook_id());

			bookCirculateHistory.setUser(user);
			bookCirculateHistory.setBook(book);
		}

		request.setAttribute("bookCirculateHistoryList", bookCirculateHistoryList);

		// circulationHistory.jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/circulationHistory.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
