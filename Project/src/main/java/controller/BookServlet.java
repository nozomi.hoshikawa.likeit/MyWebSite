package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AuthorDao;
import dao.BookDao;
import model.Author;
import model.Book;
import model.User;

@WebServlet("/BookServlet")
public class BookServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public BookServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// もしセッションがなかったら、LoginServletにリダイレクトさせる
		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// 操作を行うユーザーの情報を受け取る
		User sessionTargetUser = (User) session.getAttribute("sessionTargetUser");
		request.setAttribute("sessionTargetUser", sessionTargetUser);

		// book.jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/book.jsp");
		dispatcher.forward(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		// jspから入力された文字をもらう
		String category_select = request.getParameter("category_select");
		String keyword = request.getParameter("keyword");

		BookDao bookDao = new BookDao();

		List<Book> bookList = null;
		if (category_select.equals("title")) {
			bookList = bookDao.SearchBookFromTitle(keyword);

		} else if (category_select == "author") {
			bookList = bookDao.SearchBookFromAuthor(keyword);
		}

		for (Book book : bookList) {
			AuthorDao authorDao = new AuthorDao();
			Author author = authorDao.getAuthor(book);

			book.setAuthor(author);
		}

		// リクエストスコープに蔵書情報をセット
		request.setAttribute("bookList", bookList);

		// book.jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/book.jsp");
		dispatcher.forward(request, response);
	}

}
