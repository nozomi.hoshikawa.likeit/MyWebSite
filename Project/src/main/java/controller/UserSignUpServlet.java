package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/UserSignUpServlet")
public class UserSignUpServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserSignUpServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// もしセッションが無かったら、LoginServletにリダイレクトさせる
		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser == null) {
			response.sendRedirect("LoginServlet");
			return;
		}
		// userSignUp.jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignUp.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// リクエストパラメータの文字コード指定
		request.setCharacterEncoding("UTF-8");

		// リクエストパラメータの入力項目を取得
		String name = request.getParameter("name");
		String mail = request.getParameter("mail");
		String password = request.getParameter("password");
		String passwordConfirm = request.getParameter("passwordConfirm");

		// 一つでも未入力の項目があった場合のエラー
		if (name == "" || mail == "" || password == "" || passwordConfirm == "") {
			request.setAttribute("errMsg", "未入力の項目があります");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignUp.jsp");
			dispatcher.forward(request, response);
			return;
		}
		// passwordがpassword(確認)と異なる場合のエラー
		if (!password.equals(passwordConfirm)) {
			request.setAttribute("errMsg", "PasswordとPassword(確認)が異なっています");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignUp.jsp");
			dispatcher.forward(request, response);
			return;
		}

		// リクエストパラメータの入力項目を引数に渡してDaoのメソッドを実行
		User User = new User(name, mail, password);
		UserDao userDao = new UserDao();

		User user = userDao.findByLoginInfo(mail);

		// 既に登録されているEmailを入力した場合のエラー
		if (user != null) {
			request.setAttribute("errMsg", "既に登録されたEmailアドレスです");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSignUp.jsp");
			dispatcher.forward(request, response);
			return;

		} else {
			userDao.InsertUser(User);
			response.sendRedirect("Circulation_ReserveListServlet");
		}

	}

}
