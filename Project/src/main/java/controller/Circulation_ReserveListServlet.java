package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookCirculationDao;
import dao.BookDao;
import dao.UserDao;
import model.Book;
import model.BookCirculation;
import model.User;

@WebServlet("/Circulation_ReserveListServlet")
public class Circulation_ReserveListServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Circulation_ReserveListServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// もしセッションがなかったら、LoginServletにリダイレクトさせる
		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// 操作を行うユーザーの情報を受け取る
		User sessionTargetUser = (User) session.getAttribute("sessionTargetUser");
		request.setAttribute("sessionTargetUser", sessionTargetUser);

		// 貸出リストを呼び出す
		BookCirculationDao bookCirculationDao = new BookCirculationDao();
		List<BookCirculation> bookCirculateList = bookCirculationDao.getNotReturnedBookCirculateList();

		UserDao userDao = new UserDao();
		BookDao bookDao = new BookDao();
		for (BookCirculation bookCirculate : bookCirculateList) {
			User user = userDao.getUser(bookCirculate.getUser_id());
			Book book = bookDao.getBook(bookCirculate.getBook_id());

			bookCirculate.setUser(user);
			bookCirculate.setBook(book);
		}

		// リクエストスコープに貸出情報をセット
		request.setAttribute("bookCirculateList", bookCirculateList);

		// circulation_reserveList.jsp にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/circulation_reserveList.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

	}

}
