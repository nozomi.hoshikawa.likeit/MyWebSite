package controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/CirculationServlet")
public class CirculationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CirculationServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// ユーザーのIDを受け取る
		String userId = request.getParameter("userSearch");

		// Daoを実行してUserの情報をもらう
		UserDao userDao = new UserDao();
		User sessionTargetUser = userDao.findbyUserId(userId);

		HttpSession session = request.getSession();
		session.setAttribute("sessionTargetUser", sessionTargetUser);

		// BookServletにリダイレクト
		response.sendRedirect("BookServlet");

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

	}

}
