package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.User;

@WebServlet("/UserSearchServlet")
public class UserSearchServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public UserSearchServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// もしセッションがなかったら、LoginServletにリダイレクトさせる
		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// userSearch.jsp にフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSearch.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String user_search = request.getParameter("user_search");

		UserDao userDao = new UserDao();
		List<User> userList = userDao.findByUserNameOrMail(user_search);

		// テーブルに該当のデータ無し（ユーザー検索にヒットしなかった場合）
		if (userList.isEmpty()) {
			request.setAttribute("errMsg", "該当のユーザーが見つかりませんでした");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSearch.jsp");
			dispatcher.forward(request, response);
			return; //userSearch.jspにフォワード
		}

		// リクエストスコープにユーザー情報をセット
		request.setAttribute("userList", userList);

		// userSearch.jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSearch.jsp");
		dispatcher.forward(request, response);

	}

}
