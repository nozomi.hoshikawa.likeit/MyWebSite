package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.AuthorDao;
import dao.BookDao;
import dao.BookshelfDao;
import dao.PublisherDao;
import model.Author;
import model.Book;
import model.Bookshelf;
import model.Publisher;
import model.User;

@WebServlet("/BookDetailServlet")
public class BookDetailServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public BookDetailServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// もしセッションがなかったら、LoginServletにリダイレクトさせる
		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// 操作を行うユーザーの情報を受け取る
		User sessionTargetUser = (User) session.getAttribute("sessionTargetUser");
		request.setAttribute("sessionTargetUser", sessionTargetUser);

		String book_id = request.getParameter("book_id");

		BookDao bookDao = new BookDao();
		Book book = bookDao.getBook(Integer.parseInt(book_id));

		AuthorDao authorDao = new AuthorDao();
		Author author = authorDao.getAuthor(book);
		book.setAuthor(author);

		BookshelfDao bookshelfDao = new BookshelfDao();
		Bookshelf bookshelf = bookshelfDao.getBookshelf(book);
		book.setBookshelf(bookshelf);

		PublisherDao publisherDao = new PublisherDao();
		Publisher publisher = publisherDao.getPublisher(book);
		book.setPublisher(publisher);

		// リクエストスコープに蔵書情報をセット
		request.setAttribute("book", book);

		// bookDetail.jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/bookDetail.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

	}

}
