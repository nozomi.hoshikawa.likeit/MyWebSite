package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookCirculationDao;
import model.User;

@WebServlet("/BookReturnServlet")
public class BookReturnServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public BookReturnServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// もしセッションがなかったら、LoginServletにリダイレクトさせる
		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// 操作を行うユーザーの情報を受け取る
		User sessionTargetUser = (User) session.getAttribute("sessionTargetUser");
		request.setAttribute("sessionTargetUser", sessionTargetUser);

		// circulation_reserveList.jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/circulation_reserveList.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String bookCirculateId = request.getParameter("bookCirculateId");

		BookCirculationDao bookCirculationDao = new BookCirculationDao();
		bookCirculationDao.bookReturn(bookCirculateId);

		// circulation_reserveListServletへリダイレクト
		response.sendRedirect("Circulation_ReserveListServlet");
	}

}
