package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookCirculationDao;
import model.User;

@WebServlet("/BookCirculationServlet")
public class BookCirculationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public BookCirculationServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// もしセッションがなかったら、LoginServletにリダイレクトさせる
		HttpSession session = request.getSession();
		User sessionUser = (User) session.getAttribute("userInfo");
		if (sessionUser == null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// 操作を行うユーザーの情報を受け取る
		User sessionTargetUser = (User) session.getAttribute("sessionTargetUser");
		request.setAttribute("sessionTargetUser", sessionTargetUser);

		// circulation_reserveList.jspへフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/circulation_reserveList.jsp");
		dispatcher.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		request.setCharacterEncoding("UTF-8");

		String book_id = request.getParameter("book_id");
		HttpSession session = request.getSession();
		User sessionTargetUser = (User) session.getAttribute("sessionTargetUser");

		// ユーザーが選択されていない状態で貸出ボタンを押下した場合
		if (sessionTargetUser == null) {
			request.setAttribute("errMsg", "貸出を行うユーザーを選択してください");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userSearch.jsp");
			dispatcher.forward(request, response);
			return; //userSearch.jspにフォワード
		}

		BookCirculationDao bookCirculationDao = new BookCirculationDao();

		boolean canMakeBookCirculation = bookCirculationDao.CanMakeBookCirculation(book_id);
		if (canMakeBookCirculation) {
			bookCirculationDao.BookCirculate(book_id, sessionTargetUser);
		} else {
			request.setAttribute("errMsg", "貸出中の図書です");
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/book.jsp");
			dispatcher.forward(request, response);
			return; //book.jspにフォワード
		}

		// circulation_reserveListServletへリダイレクト
		response.sendRedirect("Circulation_ReserveListServlet");
	}

}
