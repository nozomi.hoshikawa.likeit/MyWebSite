<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ja">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<!-- fontawesome CSS -->
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">

<!-- Bundle -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

<title>蔵書検索</title>
</head>

<body class="mt-5 pt-5">

	<div class="container">

		<!-- ヘッダー -->
		<nav class="bg-light fixed-top list-unstyled navbar navbar-light navbar-expand-lg">
			<ul class="d-flex flex-row navbar-nav w-100">
				<li class="m-auto me-0 ms-0 nav-item"><a class="ms-4 navbar-brand" href="Bookservlet">図書管理システム</a></li>
				<li class="m-auto nav-item text-center"><a class="nav-link" href="UserSearchServlet">選択中の貸出予約ユーザー<br>
					<i class="far fa-user fw-bold"><span class="ps-2"><c:out value="${sessionTargetUser.name}"></c:out> (user id : <c:out value="${sessionTargetUser.id}"></c:out>)</span></i></a>
				</li>
				
				<button class="collapsed ms-auto navbar-toggler" type="button" data-toggle="collapse" data-target="#responsiveMenu" aria-controls="responsiveMenu" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="responsiveMenu">
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="BookServlet"><b>蔵書検索</b></a></li>
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="Circulation_ReserveListServlet">貸出・予約一覧</a></li>
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="CirculationHistoryServlet">貸出履歴</a></li>
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="UserServlet">ユーザー管理</a></li>
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="LogoutServlet"><i class="fas fa-sign-out-alt"><span class="ps-1 fw-light">ログアウト</span></i></a></li>
				</div>
			</ul>
		</nav>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
		<!-- 蔵書検索 -->
		<form action="BookServlet" method="post">
			<div class="input-group m-auto mb-3 mt-5 w-75">
				<select class="form-select text-center" aria-label="category select"
					name="category_select">
					<option selected>Category Select</option>
					<option value="title">タイトル</option>
					<option value="author">著者</option>
				</select> <input type="text" class="form-control w-50" placeholder="Keyword"
					aria-label="text box" id="keyword" name="keyword">
				<button class="btn btn-primary" type="submit">
					<i class="fas fa-search"></i>
				</button>
			</div>
		</form>

		<!-- フィルター -->
		<form action="BookServlet" method="post">
			<div class="m-auto me-0 mt-5 pt-5 w-25">
				<select class="form-select rounded-pill text-center"
					aria-label="filter" name="filter">
					<option selected>Filter</option>
					<option value="Java">JAVA</option>
					<option value="others_lang">その他言語</option>
					<option value="test">テスト対策</option>
				</select>
			</div>
		</form>

		<hr class="mt-0">

		<!-- 検索結果 -->
		<c:forEach var="book" items="${bookList}">
			<div class="list-group">
				<div class="border-5 border-bottom-0 border-end-0 border-top-0 list-group-item list-group-item-action m-5 m-auto mt-5 p-3 row w-75">
					<span>資料コード：${book.id}<br> タイトル：${book.title}<br>
						著者：${book.getAuthor().getName()}
						<%-- <br> 出版社${book.publisher}<br>--%>
					</span>
					<div class="d-flex d-grid gap-2 justify-content-end">
						<button class="btn btn-outline-primary me-md-2" name="book_id">
							<a href="BookDetailServlet?book_id=${book.id}">参照</a>
						</button>
						<form action="BookCirculationServlet" method="post">
							<button class="btn btn-primary me-md-2" type="submit" name="book_id" value="${book.id}">貸出</button>
						</form>
						<form action="" method="get">
							<button class="btn btn-primary" type="button" href="#">予約</button>
						</form>
					</div>
				</div>
			</div>
		</c:forEach>
		
	</div>


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>