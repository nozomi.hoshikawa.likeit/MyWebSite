<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ja">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<!-- fontawesome CSS -->
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">

<title>ユーザー管理</title>
</head>

<body class="mt-5 pt-5">

	<div class="container">

		<!-- ヘッダー -->
		<nav class="bg-light fixed-top list-unstyled navbar navbar-light navbar-expand-lg">
			<ul class="d-flex flex-row navbar-nav w-100">
				<li class="m-auto me-0 ms-0 nav-item"><a class="ms-4 navbar-brand" href="BookServlet">図書管理システム</a>
				</li>
				<li class="m-auto nav-item text-center"><a class="nav-link" href="UserSearchServlet">選択中の貸出予約ユーザー<br>
					<i class="far fa-user fw-bold"><span class="ps-2"><c:out value="${sessionTargetUser.name}"></c:out> (user id : <c:out value="${sessionTargetUser.id}"></c:out>)</span></i></a>
				</li>
				<button class="collapsed ms-auto navbar-toggler" type="button" data-toggle="collapse" data-target="#responsiveMenu" aria-controls="responsiveMenu" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="responsiveMenu">
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="BookServlet">蔵書検索</a></li>
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="Circulation_ReserveListServlet">貸出・予約一覧</a></li>
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="CirculationHistoryServlet">貸出履歴</a></li>
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="UserServlet"><b>ユーザー管理</b></B></a></li>
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="LogoutServlet"><i class="fas fa-sign-out-alt"><span class="ps-1 fw-light">ログアウト</span></i></a></li>
				</div>
			</ul>
		</nav>

		<!-- ユーザーリスト -->
		<h3 class="text-center m-3">ユーザー管理</h3>
		<c:forEach var="user" items="${userList}">
			<ul class="list-group list-group-flush m-auto w-75">
				<li class="list-group-item list-group-item list-group-item-action">
					<span class="fw-light text-black-50">ID : ${user.id}</span><br>
					<span class="fw-light">User Name : ${user.name}</span>
					<div class="d-flex justify-content-end">
					<form action="CirculationServlet" method="get">
						<button type="submit" class="btn btn-outline-primary" name="userSearch" id="userSearch" value="${user.id}">貸出・予約ユーザーへ設定</button>
					</form>
						<button type="submit" class="btn btn-outline-primary ms-1" href="#">編集</button>
						<button type="submit" class="btn btn-outline-danger ms-1" href="#">削除</button>
					</div>
				</li>
			</ul>
		</c:forEach>
		
		<!-- 新規登録ボタン -->
		<a class="btn btn-outline-primary btn-sm d-table m-auto me-0 mt-5" href="UserSignUpServlet" role="button">
			<i class="fas fa-user-plus"><span class="ps-2">ユーザーを追加</span></i>
		</a>
		
	</div>
	
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>