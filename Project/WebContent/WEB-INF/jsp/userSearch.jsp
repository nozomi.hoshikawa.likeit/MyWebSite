<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ja">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<!-- fontawesome CSS -->
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">

<!-- Bundle -->
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

<title>ユーザー検索</title>
</head>

<body class="mt-5 pt-5">

	<div class="container">

		<!-- ヘッダー -->
		<nav class="bg-light fixed-top list-unstyled navbar navbar-light navbar-expand-lg">
			<ul class="d-flex flex-row navbar-nav w-100">
				<li class="m-auto me-0 ms-0 nav-item"><a
					class="ms-4 navbar-brand" href="BookServlet">図書管理システム</a></li>
				<li class="m-auto nav-item text-center"><a class="nav-link" href="UserSearchServlet"><b>選択中の貸出予約ユーザー</b><br>
					<i class="far fa-user fw-bold"><span class="ps-2"><c:out value="${sessionTargetUser.name}"></c:out> (user id : <c:out value="${sessionTargetUser.id}"></c:out>)</span></i></a>
				</li>
				<button class="collapsed ms-auto navbar-toggler" type="button" data-toggle="collapse" data-target="#responsiveMenu" aria-controls="responsiveMenu" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse" id="responsiveMenu">
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="BookServlet">蔵書検索</a></li>
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="Circulation_ReserveListServlet">貸出・予約一覧</a></li>
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="CirculationHistoryServlet">貸出履歴</a></li>
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="UserServlet">ユーザー管理</a></li>
					<li class="ms-3 nav-item text-nowrap"><a class="nav-link" href="login.html"><i class="fas fa-sign-out-alt"><span class="ps-1 fw-light">ログアウト</span></i></a></li>
				</div>
			</ul>
		</nav>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
		<!-- ユーザー検索 -->
		<form action="UserSearchServlet" method="post">
			<div class="input-group m-auto mt-5 mb-3 w-50">
				<input type="text" class="form-control" placeholder="Email  or  User Name" name="user_search" aria-label="UserSearch" aria-describedby="button-addon2">
				<button class="btn btn-primary" type="submit">
					<i class="fas fa-search"></i>
				</button>
			</div>
		</form>

		<!-- 検索結果 -->
		<form action="CirculationServlet" method="get">
			<c:forEach var="user" items="${userList}">
				<div class="list-group m-auto mt-5 w-50">
					<div class="list-group-item-action form-check">
						<input type="radio" class="form-check-input" name="userSearch" id="userSearch${user.id}" value="${user.id}">
						<label for="userSearch${user.id}" class="form-check-label">${user.name} / ${user.mail}</label>
					</div>
				</div>
			</c:forEach>

			<!-- ユーザー決定ボタン -->
			<button class="btn btn-primary d-table m-auto mt-5 pe-5 ps-5 rounded-pill" type="submit" role="button">
			このユーザーで貸出・予約を行う
			</button>
		</form>

	</div>


	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>