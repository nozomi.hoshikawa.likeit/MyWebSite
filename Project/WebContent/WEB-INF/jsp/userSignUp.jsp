<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html lang="ja">

<head>
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- Bootstrap CSS -->
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

<!-- fontawesome CSS -->
<link href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" rel="stylesheet">

<title>新規登録</title>
</head>

<body class="mt-5 pt-5">

	<div class="container">

		<!-- ヘッダー -->
		<nav class="bg-light fixed-top list-unstyled navbar navbar-light navbar-expand-lg">
			<ul class="d-flex flex-row navbar-nav w-100">
				<li class="m-auto me-0 ms-0 nav-item"><a class="ms-4 navbar-brand" href="#">図書管理システム</a></li>
			</ul>
		</nav>

		<c:if test="${errMsg != null}">
			<div class="alert alert-danger" role="alert">${errMsg}</div>
		</c:if>
		<!-- 新規登録フォーム -->
		<form class="mt-5" action="UserSignUpServlet" method="post">
			<div class="row mb-4">
				<label for="disabledTextInput" class="col-3 form-label">User Name</label>
				<div class="col-9">
					<div class="d-flex m-auto">
						<input class="form-control" type="text" placeholder="" id="name" name="name">
					</div>
				</div>
			</div>
			<div class="row mt-3 mb-4">
				<label for="disabledTextInput" class="col-3 form-label">Email</label>
				<div class="col-9">
					<div class="d-flex m-auto">
						<input class="form-control" type="text" placeholder="" id="mail" name="mail">
					</div>
				</div>
			</div>
			<div class="row mt-3 mb-4">
				<label for="disabledTextInput" class="col-3 form-label">Password</label>
				<div class="col-9">
					<div class="d-flex m-auto">
						<input class="form-control" type="password" placeholder="" id="password" name="password">
					</div>
				</div>
			</div>
			<div class="row mt-3 mb-4">
				<label for="disabledTextInput" class="col-3 form-label">Password(確認)</label>
				<div class="col-9">
					<div class="d-flex m-auto">
						<input class="form-control" type="password" placeholder="" id="passwordConfirm" name="passwordConfirm">
					</div>
				</div>
			</div>

			<button class="btn btn-primary d-table m-auto mt-5 mb-5" type="submit" role="button">
				<i class="fas fa-user-plus"><span class="ps-2">SignUp</span></i>
			</button>
		</form>

	</div>

</body>

</html>